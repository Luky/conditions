# luky/conditions

Micro library with usefull set of conditions without loose of
readability

## Installation

```
composer require luky/conditions
```

## Usage

Any - check if **ANY** of item meets the condition

Use case examples:
*  throw exception if any element is null
*  show flash message if any validator return false

```php
if (Any::isNull($companyName, $phone, $zip)) 
{
    // do stuff
}
```

* * *

Every - check if **EVERY** elements meets the condition 

Use case examples:
* do smth if every item validation returns true

```php
if (Every::isTrue(
    
)) 
{
    // do stuff
}
```

* * *

None - check if **NONE** elements meets the condition

```php
if (Every::isTrue(

)) 
{
    // do stuff
}
```
