<?php declare(strict_types = 1);

namespace Luky\Conditions;

final class Every
{
	public static function isNull(...$args): bool
	{
		foreach ($args as &$arg) {
			if ($arg !== null) {
				return false;
			}
		}
		
		return true;
	}
	
	public static function isTrue(...$args): bool
	{
		foreach ($args as &$arg) {
			if ($arg !== true) {
				return false;
			}
		}
		
		return true;
	}
	
	public static function isFalse(...$args): bool
	{
		foreach ($args as &$arg) {
			if ($arg !== false) {
				return false;
			}
		}
		
		return true;
	}
	
	public static function instanceOf(string $class, ...$args): bool
	{
		foreach ($args as &$arg) {
			if ($arg instanceof $class === false) {
				return false;
			}
		}
		
		return true;
	}
	
	public static function isPositive(...$args): bool
	{
		foreach ($args as &$arg) {
			if ($arg <= 0) {
				return false;
			}
		}
		
		return true;
	}
	
	public static function isNegative(...$args): bool
	{
		foreach ($args as &$arg) {
			if ($arg >= 0) {
				return false;
			}
		}
		
		return true;
	}
	
	public static function isZero(...$args): bool
	{
		foreach ($args as &$arg) {
			if ($arg !== 0) {
				return false;
			}
		}
		
		return true;
	}
	
	public static function isTraversable(...$args): bool
	{
		foreach ($args as &$arg) {
			if ($arg instanceof \Traversable === false) {
				return false;
			}
		}
		
		return true;
	}
}
