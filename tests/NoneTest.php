<?php declare(strict_types = 1);

namespace Luky\Conditions\Tests;

require __DIR__ . '/bootstrap.php';

use Luky\Conditions\None;
use Tester\Assert;
use Tester\TestCase;

class NoneTest extends TestCase
{
	public function testNoneIsNull(): void
	{
		// sets
		Assert::false(None::isNull(10, 'xyz', null, new \stdClass(), [], false, true));
		Assert::true(None::isNull(10, 'xyz', new \stdClass(), [], false, true));
		
		// Order check
		Assert::false(None::isNull(true, false, null));
		Assert::false(None::isNull(true, null, false));
		Assert::false(None::isNull(false, true, null));
		Assert::false(None::isNull(false, null, true));
		Assert::false(None::isNull(null, true, false));
		Assert::false(None::isNull(null, false, true));
		
		// Arrays
		Assert::true(None::isNull([]));
		Assert::true(None::isNull([] + []));
		Assert::true(None::isNull([false]));
		Assert::true(None::isNull([null]));
		Assert::true(None::isNull([true]));
		
		// Strings
		Assert::true(None::isNull(''));
		Assert::true(None::isNull('null'));
		Assert::true(None::isNull('0'));
		Assert::true(None::isNull('1'));
		Assert::true(None::isNull('-1'));
		
		// Numbers
		Assert::true(None::isNull(0));
		Assert::true(None::isNull(1));
		Assert::true(None::isNull(-1));
		Assert::true(None::isNull(0.0));
		Assert::true(None::isNull(1.0));
		Assert::true(None::isNull(0.5 + 0.6));
		
		// Booleans
		Assert::true(None::isNull(true));
		Assert::true(None::isNull(false));
		
		// Null
		Assert::false(None::isNull(null));
		
	}
	
	public function testNoneIsFalse(): void
	{
		// sets
		Assert::false(None::isFalse(10, 'xyz', null, new \stdClass(), [], false, true));
		Assert::true(None::isFalse(10, 'xyz', null, new \stdClass(), [], true));
		
		// Order check
		Assert::false(None::isFalse(true, false, null));
		Assert::false(None::isFalse(true, null, false));
		Assert::false(None::isFalse(false, true, null));
		Assert::false(None::isFalse(false, null, true));
		Assert::false(None::isFalse(null, true, false));
		Assert::false(None::isFalse(null, false, true));
		
		// Arrays
		Assert::true(None::isFalse([]));
		Assert::true(None::isFalse([] + []));
		Assert::true(None::isFalse([false]));
		Assert::true(None::isFalse([null]));
		Assert::true(None::isFalse([true]));
		
		// Strings
		Assert::true(None::isFalse(''));
		Assert::true(None::isFalse('null'));
		Assert::true(None::isFalse('0'));
		Assert::true(None::isFalse('1'));
		Assert::true(None::isFalse('-1'));
		
		// Numbers
		Assert::true(None::isFalse(0));
		Assert::true(None::isFalse(1));
		Assert::true(None::isFalse(-1));
		Assert::true(None::isFalse(0.0));
		Assert::true(None::isFalse(1.0));
		Assert::true(None::isFalse(0.5 + 0.6));
		
		// Booleans
		Assert::true(None::isFalse(true));
		Assert::false(None::isFalse(false));
		
		// Null
		Assert::true(None::isFalse(null));
		
	}
	
	public function testNoneIsTrue(): void
	{
		// sets
		Assert::false(None::isTrue(10, 'xyz', null, new \stdClass(), [], false, true));
		
		// Order check
		Assert::false(None::isTrue(true, false, null));
		Assert::false(None::isTrue(true, null, false));
		Assert::false(None::isTrue(false, true, null));
		Assert::false(None::isTrue(false, null, true));
		Assert::false(None::isTrue(null, true, false));
		Assert::false(None::isTrue(null, false, true));
		
		// Arrays
		Assert::true(None::isTrue([]));
		Assert::true(None::isTrue([] + []));
		Assert::true(None::isTrue([false]));
		Assert::true(None::isTrue([null]));
		Assert::true(None::isTrue([true]));
		
		// Strings
		Assert::true(None::isTrue(''));
		Assert::true(None::isTrue('null'));
		Assert::true(None::isTrue('0'));
		Assert::true(None::isTrue('1'));
		Assert::true(None::isTrue('-1'));
		
		// Numbers
		Assert::true(None::isTrue(0));
		Assert::true(None::isTrue(1));
		Assert::true(None::isTrue(-1));
		Assert::true(None::isTrue(0.0));
		Assert::true(None::isTrue(1.0));
		Assert::true(None::isTrue(0.5 + 0.6));
		
		// Booleans
		Assert::false(None::isTrue(true));
		Assert::true(None::isTrue(false));
		
		// Null
		Assert::true(None::isTrue(null));
		
	}
}

(new NoneTest())->run();
