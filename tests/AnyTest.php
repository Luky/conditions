<?php declare(strict_types = 1);

namespace Luky\Conditions\Tests;

require __DIR__ . '/bootstrap.php';

use Luky\Conditions\Any;
use Tester\Assert;
use Tester\TestCase;

class AnyTest extends TestCase
{
	public function testAnyIsNull(): void
	{
		// sets
		Assert::true(Any::isNull(10, 'xyz', null, new \stdClass(), [], false, true));
		Assert::false(Any::isNull(10, 'xyz', new \stdClass(), [], false, true));
		
		// Order check
		Assert::true(Any::isNull(true, false, null));
		Assert::true(Any::isNull(true, null, false));
		Assert::true(Any::isNull(false, true, null));
		Assert::true(Any::isNull(false, null, true));
		Assert::true(Any::isNull(null, true, false));
		Assert::true(Any::isNull(null, false, true));
		
		// Arrays
		Assert::false(Any::isNull([]));
		Assert::false(Any::isNull([] + []));
		Assert::false(Any::isNull([false]));
		Assert::false(Any::isNull([null]));
		Assert::false(Any::isNull([true]));
		
		// Strings
		Assert::false(Any::isNull(''));
		Assert::false(Any::isNull('null'));
		Assert::false(Any::isNull('0'));
		Assert::false(Any::isNull('1'));
		Assert::false(Any::isNull('-1'));
		
		// Numbers
		Assert::false(Any::isNull(0));
		Assert::false(Any::isNull(1));
		Assert::false(Any::isNull(-1));
		Assert::false(Any::isNull(0.0));
		Assert::false(Any::isNull(1.0));
		Assert::false(Any::isNull(0.5 + 0.6));
		
		// Booleans
		Assert::false(Any::isNull(true));
		Assert::false(Any::isNull(false));
		
		// Null
		Assert::true(Any::isNull(null));
	}
	
	public function testAnyIsFalse(): void
	{
		// sets
		Assert::true(Any::isFalse(10, 'xyz', null, new \stdClass(), [], false, true));
		Assert::false(Any::isFalse(10, 'xyz', null, new \stdClass(), [], true));
		
		// Order check
		Assert::true(Any::isFalse(true, false, null));
		Assert::true(Any::isFalse(true, null, false));
		Assert::true(Any::isFalse(false, true, null));
		Assert::true(Any::isFalse(false, null, true));
		Assert::true(Any::isFalse(null, true, false));
		Assert::true(Any::isFalse(null, false, true));
		
		// Arrays
		Assert::false(Any::isFalse([]));
		Assert::false(Any::isFalse([] + []));
		Assert::false(Any::isFalse([false]));
		Assert::false(Any::isFalse([null]));
		Assert::false(Any::isFalse([true]));
		
		// Strings
		Assert::false(Any::isFalse(''));
		Assert::false(Any::isFalse('null'));
		Assert::false(Any::isFalse('0'));
		Assert::false(Any::isFalse('1'));
		Assert::false(Any::isFalse('-1'));
		
		// Numbers
		Assert::false(Any::isFalse(0));
		Assert::false(Any::isFalse(1));
		Assert::false(Any::isFalse(-1));
		Assert::false(Any::isFalse(0.0));
		Assert::false(Any::isFalse(1.0));
		Assert::false(Any::isFalse(0.5 + 0.6));
		
		// Booleans
		Assert::false(Any::isFalse(true));
		Assert::true(Any::isFalse(false));
		
		// Null
		Assert::false(Any::isFalse(null));
	}
	
	public function testAnyIsTrue(): void
	{
		// sets
		Assert::true(Any::isTrue(10, 'xyz', null, new \stdClass(), [], false, true));
		Assert::false(Any::isTrue(10, 'xyz', null, new \stdClass(), [], false));
		
		// Order check
		Assert::true(Any::isTrue(true, false, null));
		Assert::true(Any::isTrue(true, null, false));
		Assert::true(Any::isTrue(false, true, null));
		Assert::true(Any::isTrue(false, null, true));
		Assert::true(Any::isTrue(null, true, false));
		Assert::true(Any::isTrue(null, false, true));
		
		// Arrays
		Assert::false(Any::isTrue([]));
		Assert::false(Any::isTrue([] + []));
		Assert::false(Any::isTrue([false]));
		Assert::false(Any::isTrue([null]));
		Assert::false(Any::isTrue([true]));
		
		// Strings
		Assert::false(Any::isTrue(''));
		Assert::false(Any::isTrue('null'));
		Assert::false(Any::isTrue('0'));
		Assert::false(Any::isTrue('1'));
		Assert::false(Any::isTrue('-1'));
		
		// Numbers
		Assert::false(Any::isTrue(0));
		Assert::false(Any::isTrue(1));
		Assert::false(Any::isTrue(-1));
		Assert::false(Any::isTrue(0.0));
		Assert::false(Any::isTrue(1.0));
		Assert::false(Any::isTrue(0.5 + 0.6));
		
		// Booleans
		Assert::true(Any::isTrue(true));
		Assert::false(Any::isTrue(false));
		
		// Null
		Assert::false(Any::isTrue(null));
	}
	
	public function testAnyIsNotTrue(): void
	{
		// sets
		Assert::true(Any::notTrue(10, 'xyz', null, new \stdClass(), [], false, true));
		Assert::false(Any::notTrue(true, true, true, true, true, true));
		Assert::true(Any::notTrue(true, true, true, true, true, false));
		Assert::true(Any::notTrue(true, true, false, true, true, true));
		
		// Order check
		Assert::true(Any::notTrue(null, 0, 1));
		Assert::true(Any::notTrue(true, null, false));
		Assert::true(Any::notTrue(false, true, null));
		Assert::true(Any::notTrue(false, null, true));
		Assert::true(Any::notTrue(null, true, false));
		Assert::true(Any::notTrue(null, false, true));
		
		
		// Arrays
		Assert::true(Any::notTrue(true, []));
		Assert::true(Any::notTrue([] + [], true));
		Assert::true(Any::notTrue([false]));
		Assert::true(Any::notTrue([null]));
		Assert::true(Any::notTrue([true]));
		
		// Strings
		Assert::true(Any::notTrue(''));
		Assert::true(Any::notTrue('null'));
		Assert::true(Any::notTrue('0'));
		Assert::true(Any::notTrue('1'));
		Assert::true(Any::notTrue('-1'));
		
		// Numbers
		Assert::true(Any::notTrue(0));
		Assert::true(Any::notTrue(1));
		Assert::true(Any::notTrue(-1));
		Assert::true(Any::notTrue(0.0));
		Assert::true(Any::notTrue(1.0));
		Assert::true(Any::notTrue(0.5 + 0.6));
		
		// Booleans
		Assert::false(Any::notTrue(true));
		Assert::true(Any::notTrue(false));
		
		// Null
		Assert::true(Any::notTrue(null));
	}
}

(new AnyTest())->run();
