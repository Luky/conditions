<?php declare(strict_types = 1);

namespace Luky\Conditions\Tests;

require __DIR__ . '/bootstrap.php';

use Luky\Conditions\Every;
use Tester\Assert;
use Tester\TestCase;

class EveryTest extends TestCase
{
	public function testEveryIsNull(): void
	{
		// sets
		Assert::false(Every::isNull(10, 'xyz', null, new \stdClass(), [], false, true));
		Assert::true(Every::isNull(null, null, null, null, null, null));
		Assert::false(Every::isNull(null, null, false, null, null, null));
		Assert::false(Every::isNull(null, null, true, null, null, null));
		Assert::false(Every::isNull(null, null, '', null, null, null));
		Assert::false(Every::isNull(null, null, null, 0, null, null));
		Assert::false(Every::isNull(null, null, null, 0, null, false));
		
		// Order check
		Assert::false(Every::isNull(true, false, null));
		Assert::false(Every::isNull(true, null, false));
		Assert::false(Every::isNull(false, true, null));
		Assert::false(Every::isNull(false, null, true));
		Assert::false(Every::isNull(null, true, false));
		Assert::false(Every::isNull(null, false, true));
		
		// Arrays
		Assert::false(Every::isNull([]));
		Assert::false(Every::isNull([] + []));
		Assert::false(Every::isNull([false]));
		Assert::false(Every::isNull([null]));
		Assert::false(Every::isNull([true]));
		
		// Strings
		Assert::false(Every::isNull(''));
		Assert::false(Every::isNull('null'));
		Assert::false(Every::isNull('0'));
		Assert::false(Every::isNull('1'));
		Assert::false(Every::isNull('-1'));
		
		// Numbers
		Assert::false(Every::isNull(0));
		Assert::false(Every::isNull(1));
		Assert::false(Every::isNull(-1));
		Assert::false(Every::isNull(0.0));
		Assert::false(Every::isNull(1.0));
		Assert::false(Every::isNull(0.5 + 0.6));
		
		// Booleans
		Assert::false(Every::isNull(true));
		Assert::false(Every::isNull(false));
		
		// Null
		Assert::true(Every::isNull(null));
	}
	
	public function testEveryIsFalse(): void
	{
		// sets
		Assert::false(Every::isFalse(10, 'xyz', null, new \stdClass(), [], false, true));
		
		Assert::true(Every::isFalse(false, false, false, false, false, false, false));
		
		Assert::false(Every::isFalse(false, false, false, false, false, false, true));
		Assert::false(Every::isFalse(false, false, false, true, false, false, false));
		Assert::false(Every::isFalse(false, false, false, null, false, false, false));
		Assert::false(Every::isFalse(false, false, false, 0, false, false, false));
		Assert::false(Every::isFalse(false, false, false, 1, false, false, false));
		Assert::false(Every::isFalse(null, null, null, null, null, null, null, null));
		Assert::false(Every::isFalse(true, true, true, true, true, true, true, true));
		
		// Order check
		Assert::false(Every::isFalse(true, false, null));
		Assert::false(Every::isFalse(true, null, false));
		Assert::false(Every::isFalse(false, true, null));
		Assert::false(Every::isFalse(false, null, true));
		Assert::false(Every::isFalse(null, true, false));
		Assert::false(Every::isFalse(null, false, true));
		
		// Arrays
		Assert::false(Every::isFalse([]));
		Assert::false(Every::isFalse([] + []));
		Assert::false(Every::isFalse([false]));
		Assert::false(Every::isFalse([null]));
		Assert::false(Every::isFalse([true]));
		
		// Strings
		Assert::false(Every::isFalse(''));
		Assert::false(Every::isFalse('null'));
		Assert::false(Every::isFalse('0'));
		Assert::false(Every::isFalse('1'));
		Assert::false(Every::isFalse('-1'));
		
		// Numbers
		Assert::false(Every::isFalse(0));
		Assert::false(Every::isFalse(1));
		Assert::false(Every::isFalse(-1));
		Assert::false(Every::isFalse(0.0));
		Assert::false(Every::isFalse(1.0));
		Assert::false(Every::isFalse(0.5 + 0.6));
		
		// Booleans
		Assert::false(Every::isFalse(true));
		Assert::true(Every::isFalse(false));
		
		// Null
		Assert::false(Every::isFalse(null));
	}
	
	public function testEveryIsTrue(): void
	{
		// sets
		Assert::false(Every::isTrue(10, 'xyz', null, new \stdClass(), [], false, true));
		
		// Order check
		Assert::false(Every::isTrue(true, false, null));
		Assert::false(Every::isTrue(true, null, false));
		Assert::false(Every::isTrue(false, true, null));
		Assert::false(Every::isTrue(false, null, true));
		Assert::false(Every::isTrue(null, true, false));
		Assert::false(Every::isTrue(null, false, true));
		
		// Arrays
		Assert::false(Every::isTrue([]));
		Assert::false(Every::isTrue([] + []));
		Assert::false(Every::isTrue([false]));
		Assert::false(Every::isTrue([null]));
		Assert::false(Every::isTrue([true]));
		
		// Strings
		Assert::false(Every::isTrue(''));
		Assert::false(Every::isTrue('null'));
		Assert::false(Every::isTrue('0'));
		Assert::false(Every::isTrue('1'));
		Assert::false(Every::isTrue('-1'));
		
		// Numbers
		Assert::false(Every::isTrue(0));
		Assert::false(Every::isTrue(1));
		Assert::false(Every::isTrue(-1));
		Assert::false(Every::isTrue(0.0));
		Assert::false(Every::isTrue(1.0));
		Assert::false(Every::isTrue(0.5 + 0.6));
		
		// Booleans
		Assert::true(Every::isTrue(true));
		Assert::false(Every::isTrue(true, true, 1));
		Assert::false(Every::isTrue(false));
		
		// Null
		Assert::false(Every::isTrue(null));
	}
}

(new EveryTest())->run();
